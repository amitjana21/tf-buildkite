# in ./steps.yml:
# steps:
#   - label: ':pipeline:'
#     command: buildkite-agent upload
resource "buildkite_pipeline" "test" {
  name       = "Test3"
  repository = "https://amitjana21@bitbucket.org/amitjana21/tf-buildkite.git"

  steps = file("./steps.yml")
}

# resource "buildkite_pipeline_schedule" "foo" {
#   pipeline_id = buildkite_pipeline.test.id
#   cronline    = "0 *  * * *"
#   label       = "My schedule"
#   branch      = "master"
# }

output "badge_url" {
  value = buildkite_pipeline.test.badge_url
}