terraform {
  required_providers {
    buildkite = {
      source  = "buildkite/buildkite"
      version = "0.5.0"
    }
  }
}

provider "buildkite" {
  # Configuration options
  api_token    = "ac71b343d8de9fdda5455a02ce4533d397678031" # can also be set from env: BUILDKITE_API_TOKEN
  organization = "amitjana21"                               # can also be set from env: BUILDKITE_ORGANIZATION
}