provider "aws" {
  region  = "us-east-2"
  profile = "default"
  #shared_credentials_file = "/Users/tf_user/.aws/creds"
}

# resource "random_id" "test" {
#   byte_length = 8
# }

resource "aws_instance" "example" {
  ami           = "ami-002068ed284fb165b"
  instance_type = "t2.micro"
}